# Not much here, this is a small package anyway
__all__ = ["anaio"]
__version__ = '1.0.2'

# Import routines
from .anaio import *
import anaio as core
